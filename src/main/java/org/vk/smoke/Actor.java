package org.vk.smoke;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.concurrent.TimeUnit;

import org.ini4j.Wini;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import com.vk.api.sdk.client.TransportClient;
import com.vk.api.sdk.client.VkApiClient;
import com.vk.api.sdk.client.actors.UserActor;
import com.vk.api.sdk.exceptions.ApiException;
import com.vk.api.sdk.exceptions.ClientException;
import com.vk.api.sdk.exceptions.OAuthException;
import com.vk.api.sdk.httpclient.HttpTransportClient;
import com.vk.api.sdk.objects.UserAuthResponse;

import junit.framework.Assert;

public class Actor {
	private static Actor actor;
	
	private static UserActor user;
	private static VkApiClient client;
	private static WebDriver driver;
	
	private static int APP_ID;
	private static String CLIENT_SECRET;
	private static String REDIRECT_URI;
	
	private static String login;
	private static String pass;
	private static String q;
	
	private static int userId;
	
	private static String OAUTH;
	
	private static String accessToken;
	
	@SuppressWarnings("deprecation")
	public static synchronized Actor getInstance() throws IOException, URISyntaxException {
		if (actor == null) {
			actor = new Actor();
		}
		
		System.setProperty("webdriver.chrome.driver", "c:/chromedriver.exe");
		driver = new ChromeDriver();
		
		Wini ini = new Wini(new File("./res/config.ini"));
        Wini.Section section = ini.get("auth");
        
        APP_ID = new Integer(section.get("APP_ID")).intValue();
        CLIENT_SECRET = section.get("CLIENT_SECRET");
        REDIRECT_URI = section.get("REDIRECT_URI");
        
        login = section.get("login");
        pass = section.get("pass");
        
        userId = new Integer(section.get("userId")).intValue();
        
        q = section.get("Q");
        
        OAUTH = "https://oauth.vk.com/authorize?client_id=" + APP_ID 
    			+ "&display=page&redirect_uri=" + REDIRECT_URI + "&scope=messages&res";
		
		try {
			accessToken = askToken(driver, OAUTH);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		TransportClient transportClient = HttpTransportClient.getInstance(); 
        client = new VkApiClient(transportClient); 
        
        UserAuthResponse authResponse = null;
		try {
			authResponse = client.oauth() 
				    .userAuthorizationCodeFlow(APP_ID, CLIENT_SECRET, REDIRECT_URI, accessToken) 
				    .execute();
		} catch (OAuthException e) { 
			e.getRedirectUri(); 
		} catch (ApiException e) {
			e.printStackTrace();
		} catch (ClientException e) {
			e.printStackTrace();
		}
		
		user = new UserActor(authResponse.getUserId(), authResponse.getAccessToken()); 
		
		driver.close();
        driver.quit();
        
        return actor;
	}
	
	/**
	 * Коммуникационный объект.
	 * @return UserActor
	 */
	public UserActor getUser() {
		Assert.assertNotNull("UserActor is null.", user);
		return user;
	}
	
	/**
	 * Объект для работы с апи-вконтакте.
	 * @return VkApiClient
	 */
	public VkApiClient getVk() {
		Assert.assertNotNull("VkApiClient is null.", user);
		return client;
	}
	
	/**
	 * id стороннего пользователя, с которым ведём работу.
	 * @return int
	 */
	public int getUserId() {
		Assert.assertNotNull("UserId is null.", userId);
		return userId;
	}
	
	public String getUserQ() {
		Assert.assertNotNull("Q is null.", q);
		return q;
	}
	
	
	/* ----------------------------------<Служебные методы>---------------------------------- **/
	
	private static String askToken(WebDriver driver, String link) throws IOException, URISyntaxException, InterruptedException{
		
		driver.get(link);
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		
		WebElement email = driver.findElement(By.name("email"));
		email.sendKeys(getLogin());
		WebElement pass = driver.findElement(By.name("pass"));
		pass.sendKeys(getPass());
		pass.submit();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		
		String curr_link = driver.getCurrentUrl();
		String token = curr_link.substring(37);
		
		return token;
	}
	
	private static String getLogin() {
		Assert.assertNotNull("Login is null.", login);
		return login;
	}
	
	private static String getPass() {
		Assert.assertNotNull("Pass is null.", pass);
		return pass;
	}
}