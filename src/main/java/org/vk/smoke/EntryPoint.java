package org.vk.smoke;

import java.io.IOException;
import java.net.URISyntaxException;

import com.vk.api.sdk.client.VkApiClient;
import com.vk.api.sdk.client.actors.UserActor;

public class EntryPoint {
	
	private Actor a;
	private static UserActor actor;
	private static VkApiClient vk;
	private static int userId;
	private static String q;
	
	public EntryPoint() throws IOException, URISyntaxException {
		a = Actor.getInstance();
		actor = a.getUser();
		vk = a.getVk();
		userId = a.getUserId();
		q = a.getUserQ();
	}
	
	public static UserActor getActor() {
		return actor;
	}
	
	public static VkApiClient getVkClient() {
		return vk;
	}
	
	public static int getUserId() {
		return userId;
	}
	
	public static String getUserQ() {
		return q;
	}
}
