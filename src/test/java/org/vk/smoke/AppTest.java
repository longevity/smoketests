package org.vk.smoke;

import java.io.IOException;
import java.net.URISyntaxException;

import junit.framework.Assert;
import junit.framework.TestCase;

public class AppTest 
    extends TestCase
{
    public AppTest( String testName ){super( testName );}

    /**
     * Rigourous Test :-)
     * @throws URISyntaxException 
     * @throws IOException 
     */
    public void testApp() throws IOException, URISyntaxException
    {
        Assert.assertNotNull("Entry Point is null.", new EntryPoint());
    }
}
