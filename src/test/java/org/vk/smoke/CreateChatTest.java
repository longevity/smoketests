package org.vk.smoke;

import java.io.IOException;
import java.net.URISyntaxException;

import com.vk.api.sdk.exceptions.ApiException;
import com.vk.api.sdk.exceptions.ClientException;

import junit.framework.Assert;
import junit.framework.TestCase;

public class CreateChatTest extends TestCase {
	public CreateChatTest( String testName ){ super( testName ); }

    public void testCreateChatTest() throws IOException, URISyntaxException { 	
    	try {	
    		Assert.assertNotSame(9, EntryPoint.getVkClient()
        			.messages()
        			.createChat(
        				EntryPoint.getActor(), 
        				new int[]{-1, -1, -1}
        			)
        			.title("new chat")
        			.execute().intValue());
		} catch (ApiException e) {
			e.printStackTrace();
		} catch (ClientException e) {
			e.printStackTrace();
		}
    }
}
