package org.vk.smoke;

import junit.framework.TestCase;

import java.io.IOException;
import java.net.URISyntaxException;

import com.vk.api.sdk.exceptions.ApiException;
import com.vk.api.sdk.exceptions.ClientException;

public class MsgSenderTest extends TestCase {
    public MsgSenderTest( String testName ){ super( testName ); }

    public void testMsgSender() throws IOException, URISyntaxException
    { 	
    	try {
    		EntryPoint.getVkClient()
    			.messages()
    			.send(EntryPoint.getActor())
    			.userId(EntryPoint.getUserId())
    			.message("_+_0_+_")
    			.execute();
		} catch (ApiException e) {
			e.printStackTrace();
		} catch (ClientException e) {
			e.printStackTrace();
		}
    }
}
