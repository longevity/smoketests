package org.vk.smoke;

import java.io.IOException;
import java.net.URISyntaxException;

import com.vk.api.sdk.exceptions.ApiException;
import com.vk.api.sdk.exceptions.ClientException;
import com.vk.api.sdk.objects.messages.responses.GetHistoryResponse;

import junit.framework.Assert;
import junit.framework.TestCase;

public class MsgTReceivedTest extends TestCase {
    public MsgTReceivedTest( String testName ){ super( testName );}

    public void testMsgReceived() throws IOException, URISyntaxException
    {
    	try {
    		GetHistoryResponse hist = 
    			EntryPoint.getVkClient()
    				.messages()
    				.getHistory(EntryPoint.getActor())
    				.userId(EntryPoint.getUserId())
    				.offset(0)
    				.count(1)
    				.startMessageId(-1)
    				.execute();
    		hist.getItems().forEach(s->System.out.println("Item : " + s) );
    		String last_msg = hist.getItems().get(0).getBody();
    		System.out.println("last message : " + last_msg);

			Assert.assertEquals("_+_0_+_", last_msg);
		} catch (ApiException e) {
			e.printStackTrace();
		} catch (ClientException e) {
			e.printStackTrace();
		}
    }
}
