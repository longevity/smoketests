package org.vk.smoke;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import com.vk.api.sdk.exceptions.ApiException;
import com.vk.api.sdk.exceptions.ClientException;
import com.vk.api.sdk.objects.messages.responses.SearchDialogsResponse;

import junit.framework.Assert;
import junit.framework.TestCase;

public class SearchUserChatTest extends TestCase {
	public SearchUserChatTest( String testName ){ super( testName ); }

    public void testSearchUserChatTest() throws IOException, URISyntaxException
    { 	
    	try {
    		List<SearchDialogsResponse> dialogs =
    		EntryPoint.getVkClient()
    			.messages()
    			.searchDialogs(EntryPoint.getActor())
    			.q(EntryPoint.getUserQ())
    			.limit(1)
    			.execute();
    		Assert.assertEquals(1, dialogs.size());
		} catch (ApiException e) {
			e.printStackTrace();
		} catch (ClientException e) {
			e.printStackTrace();
		}
    }
}
