package org.vk.smoke;

import java.io.IOException;
import java.net.URISyntaxException;

import com.vk.api.sdk.exceptions.ApiException;
import com.vk.api.sdk.exceptions.ClientException;
import com.vk.api.sdk.objects.messages.responses.GetDialogsResponse;

import junit.framework.Assert;
import junit.framework.TestCase;

public class UserDialogsSearchTest extends TestCase {
	public UserDialogsSearchTest( String testName ){ super( testName );}

    public void testUserUnreadDialogSearchTest() throws IOException, URISyntaxException
    {
    	try {
    		GetDialogsResponse unreadDialog =
    			EntryPoint.getVkClient()
    				.messages()
    				.getDialogs(EntryPoint.getActor())
    				.unread(true)
    				.execute();

    		Assert.assertEquals(1, unreadDialog.getCount().intValue());
    		
    		String unread_msg = unreadDialog.getItems().get(0).getMessage().getBody();
    		
    		System.out.println("unread msg (body) : " + unread_msg);
    
			Assert.assertEquals("unread msg", unread_msg.trim());
		} catch (ApiException e) {
			e.printStackTrace();
		} catch (ClientException e) {
			e.printStackTrace();
		}
    }
    
    public void testUserDialogsSearchTest() throws IOException, URISyntaxException
    {
    	try {
    		GetDialogsResponse dialogs =
    			EntryPoint.getVkClient()
    				.messages()
    				.getDialogs(EntryPoint.getActor())
    				.unread(false)
    				.execute();

    		Assert.assertEquals(87, dialogs.getCount().intValue());
		} catch (ApiException e) {
			e.printStackTrace();
		} catch (ClientException e) {
			e.printStackTrace();
		}
    }
}
